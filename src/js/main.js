$(document).ready(function(){

  $(".features").owlCarousel({

      navigation : false, // Show next and prev buttons
      slideSpeed : 500,
      paginationSpeed : 400,
      singleItem:true,
      mouseDrag: false,
      loop: true,
      //autoPlay: true

  });

  owl = $('.features').owlCarousel();
  $(".prev").click(function () {
      owl.trigger('owl.prev');
  });

  $(".next").click(function () {
      owl.trigger('owl.next');
  });

});
